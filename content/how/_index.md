---
title: How Does Commons Work?
weight: 2
---

# How Does Commons Work?

{{< hint primary >}}
"The common is not to be construed, therefore, as a particular kind of thing, asset or even social process, but as an unstable and malleable social relation between a particular self-defined social group and those aspects of its actually existing or yet-to-be-created social and/or physical environment deemed crucial to its life and livelihood. There is, in effect, a social practice of commoning. This practice produces or establishes a social relation with a common whose uses are either exclusive to a social group or partially or fully open to all and sundry. At the heart of the practice of commoning lies the principle that the relation between the social group and that aspect of the environment being treated as a common shall be both collective and non-commodified-off-limits to the logic of market exchange and market valuations." ~David Harvey
{{< /hint >}}

Commons come into existence through commoning. Commoning is the practice of realizing the value and importance of a particular kind of commons and working towards that.

This process is certainly not automatic. But it isn't completely in anybody's control either. It is an organic growth compelled by the value of the commons.

For example, let us take [Prav](https://prav.app/). Having a private, secure, and decentralized communication service is a felt need for many people. Every person having to self-host such a service is a pain. On the other hand, if a group of people come together to host a service like that, the pain gets shared. And so does the benefit! That is what commons does. It helps everyone.

Obviously things start rolling only when a few individuals convince each other to work together. But once it starts rolling, it becomes a self-sustaining force.

People come in and share effort, experience, and resources. People in return get what they value. It is a process of give and take based on cooperation and sharing. Based on love and kindness.

---

Now read about the [Challenges That Commons Face](../challenges/)