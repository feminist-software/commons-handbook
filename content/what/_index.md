---
title: What is Commons?
weight: 1
---

# What is Commons?

It is easy to find examples of commons all around us. The air we breathe, the waves in the beaches, the rivers, the mountains, a lot of things in nature are automatically "commons".

## What are some defining features of commons?

Let's take a joggers park near your home. Is it an example of the commons? You wouldn't be wrong to say that it is. The government might own and maintain it, but it is open to everyone and everyone has a duty to keep it tidy.

What about forest? Forest-dwelling tribes in India have long tradition of treating the forest as the commons. They take from the forest what they need and let the forest be.

Perhaps, then, the defining features of commons are:

- The commons is for everyone.
- The commons is *for* nobody.

{{< hint info >}}
“The world has enough for everyone's need, but not enough for everyone's greed.” - MK Gandhi.
{{< /hint >}}

The commons is for everyone's need. But it is for nobody's greed.

You take what you need from commons and you give what you can to commons.

## Is commons found only in nature?

No. Like Wikipedia there are many human-made commons. Cultural commons. Intellectual commons. Digital commons. Knowledge commons.

Since the internet, it has become extremely easy to create commons of various kinds.

[Prav](https://prav.app/), for example, is a commons in communication service.

---

Now read [What Commons is Not](./what-not.md) or [How Commons Works](../how/)