---
title: What Commons Is Not
---

# What Commons Is Not

It is probably easy to define commons in a negative way, as a space and community where market logic and commodification is not permitted.

But commons does not have to be defined based on absence of markets. Commons has a positive definition. One that is based on love and sharing. Commons is incomprehensible for markets because of the same reason.

## Naive folks

Commons is not a result of naiveness of people. Commons is a result of people being extremely sensitive and smart. Commons is a smart choice to be made when people are faced with multiple global challenges to human kind.

It is from a position of love, agency, power, and kindness that commons emerges.

---

Now read about [How Commons Works](../how/)