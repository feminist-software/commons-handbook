---
title: Introduction
---

# Commons Handbook

{{< hint info >}}
"The commons is the cultural and natural resources accessible to all members of a society, including natural materials such as air, water, and a habitable Earth. These resources are held in common even when owned privately or publicly. Commons can also be understood as natural resources that groups of people (communities, user groups) manage for individual and collective benefit. Characteristically, this involves a variety of informal norms and values (social practice) employed for a governance mechanism. Commons can also be defined as a social practice of governing a resource not by state or market but by a community of users that self-governs the resource through institutions that it creates."
~ **[Wikipedia](https://en.wikipedia.org/wiki/Commons)**
{{< /hint >}}

That definition is from Wikipedia. Who owns Wikipedia? One might say the [Wikimedia Foundation](https://wikimediafoundation.org/). Yes, Wikimedia Foundation hosts the encyclopedia. But the content belongs to [everyone](https://en.wikipedia.org/wiki/Wikipedia:Ownership_of_content). Anyone can edit Wikipedia.

There is the article called [India](https://en.wikipedia.org/wiki/India) on Wikipedia. The Indian government has no special right on that article. Neither has anyone else in the world. You can edit, I can edit, so can anyone else. Nobody owns anything on Wikipedia. Or, everyone owns everything on Wikipedia. It is the best example of commons that we have in front of us!

But [what](./what/) is "commons"? [How](./how/) does it form? How does it function? What are the [challenges](./challenges/) that commons face? How does it [survive](./nourishing/) those challenges? And do we need commons? These are some questions that will be addressed in this handbook.