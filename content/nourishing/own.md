---
title: Own Your Commons
weight: 1
---

# Own Your Commons

Leadership in commons is distributed.

Do not wait for permission.

Seek to build consensus - because there is power in doing things together.

If you believe that something is good for the commons, it is easy to convince others about how that is good for the commons.

Talk to others, find support, and do things. Keep doing things.

When everyone does this, everyone benefits.

Take ownership. Take initiative. And build together.

---

[Tell Your Story](./story/)