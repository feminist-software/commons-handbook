---
title: Tell Your Story
weight: 2
---

# Tell Your Story

Narratives bring human beings together. Narratives shift perspectives. Narratives move mountains.

The commons is all about the story. The folklore and the fairytale.

Share your story with passion. Inspire people in the power of the commons. Charm them through kindness.

Evangelism and advocacy fall under storytelling. Talk to people. Talk in conferences. Talk in the bus. Talk to friends. Talk to family. Write. Draw. Dance. Express your story in all ways that humans are supposed to.

---

[Build Your Tribe](./tribe/)