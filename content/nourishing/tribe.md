---
title: Build Your Tribe
weight: 3
---

# Build Your Tribe

The most beautiful thing about commons is being part of the tribe that takes care of the commons.

The commons tribe knows the meaning of love. They nurture each other to grow to their full potential.

Mentor others. Groom them. Make them believe in themselves. Make them realize that they've been aiming too less. Make them aim for the stars. And help them reach there.