---
title: How To Nourish The Commons
weight: 4
---

# How To Nourish The Commons

This is perhaps the reason why this handbook is written. Since a lot of us are not exposed to the commons approach on a day-to-day basis, it can be difficult to imagine how work nourishing the commons looks like and feels like.

There are plenty of things that can be done to nourish commons:

- [Own your commons](./own/) - leadership in commons
- [Tell your story](./story/) - the power of narratives
- [Build your tribe](./tribe/) - community building
- [...add the next thing yourself]