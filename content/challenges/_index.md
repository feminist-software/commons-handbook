---
title: Challenges That Commons Face
weight: 3
---

# Challenges That Commons Face

There is no need to romanticize commons too much either.

Like everything human, there is nothing perfect about commons.

- Commons gets abused
- Commons is vulnerable to ["tragedy of the commons"](https://en.wikipedia.org/wiki/Tragedy_of_the_commons)
- Commons is not a predominant way of thinking for a lot of people

The resilience of commons to these challenges depend on each commons and their unique challenges.

In the case of an online service, for example, it might be possible to distribute the administrative chore of identifying spammers and blocking them. It might sometimes be impossible to do so if the technology is so designed that spammers are difficult to identify.

## Tragedy of the commons

Articulated by Garett Hardin as thus:

> Therein is the tragedy. Each man is locked into a system that compels him to increase his herd without limit – in a world that is limited. Ruin is the destination toward which all men rush, each pursuing his own best interest in a society that believes in the freedom of the commons.

This is the so called "tragedy of the commons". It is not just an economic theory, there are some elements of truth to it. Individuals when left alone find it difficult to understand commons, to contribute to commons, or to maintain commons. They will nevertheless derive benefit from commons. And if there are too many such individuals, the commons will suffer too.

But there are certain assumptions that underlie tragedy of the commons:

- Individuals are left alone and there is no group thinking
- Individuals will be foolishly exploiting commons to its peril
- Individuals cannot act in the interest of the commons

These are all superficial assumptions based on market economics that doesn't understand human beings. When motivated in the right ways, human beings are extremely good with nurturing a flourishing commons.

---

[How to Nourish Commons?](../nourishing/)